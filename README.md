# Vampire

Directory RTC_Shifter includes the Eagle design files for a PCB which enables to shift the RTC module away from the Vampire V4 case.
PCB can be ordered at [OSH Park](https://oshpark.com/shared_projects/EsXF6ZsM).